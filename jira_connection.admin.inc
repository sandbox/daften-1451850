<?php

/**
 * @file
 * Jira connection module settings UI.
 */

/**
 * Provides settings pages.
 */
function jira_connection_admin_settings() {
  $form['server'] = array(
    '#type' => 'fieldset',
    '#title' => t('Jira connection server'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['server']['jira_connection_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Hostname'),
    '#default_value' => variable_get('jira_connection_server', ''),
    '#size' => 30,
    '#maxlength' => 255,
    '#description' => t('Hostname or IP Address of the CAS server.'),
  );

  $form['server']['jira_connection_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Port'),
    '#default_value' => variable_get('jira_connection_port', ''),
    '#size' => 5,
    '#maxlength' => 5,
    '#description' => t('80 is the default HTTP port. 443 is the standard SSL port.'),
  );

  $form['server']['jira_connection_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Path'),
    '#default_value' => variable_get('jira_connection_path', ''),
    '#size' => 30,
    '#description' => t('If Jira is not at the root of the host, include a path (e.g., /jira).'),
  );

  $form['server']['jira_connection_ssl_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable SSL'),
    '#default_value' => variable_get('jira_connection_ssl_enabled', FALSE),
    '#description' => t('Check if Jira should be accessed over SSL.'),
  );

  $form['server']['jira_connection_admin_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin username'),
    '#default_value' => variable_get('jira_connection_admin_username', ''),
    '#size' => 30,
    '#description' => t('A Jira admin username (preferably the primary user).'),
  );

  $form['server']['jira_connection_admin_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Admin password'),
    '#default_value' => variable_get('jira_connection_admin_password', ''),
    '#size' => 30,
    '#description' => t('The password corresponding to the username you gave above.'),
  );

  return system_settings_form($form);
}
