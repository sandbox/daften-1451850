<?php

/**
 * @file
 * Jira connection primary module file.
 */

/**
 * Implements hook_menu().
 */
function jira_connection_menu() {
  $items = array();

  $items['admin/config/people/jira_connection'] = array(
    'title' => 'Jira connection settings',
    'description' => 'Configure the connection with a Jira installation',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('jira_connection_admin_settings'),
    'access arguments' => array('administer jira connection'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'jira_connection.admin.inc',
  );

  return $items;
}

/**
 * Implementation of hook_permission().
 */
function jira_connection_permission() {
  $permissions = array();
  $permissions['administer jira connection'] = array(
    'title' => t('Administer Jira connection'),
    'description' => t('Allow users to change Jira connection settings.')
  );
  return $permissions;
}

/**
 * Implementation of hook_requirements().
 */
function jira_connection_requirements($phase) {
  $requirements = array();

  if ($phase == 'runtime') {
    if (is_null(variable_get('jira_connection_server')) || is_null(variable_get('jira_connection_admin_username'))) {
      $requirements['jira_connection_config'] = array(
        'value' => t('Not configured'),
        'severity' => REQUIREMENT_WARNING,
        'description' => t("Don't forget to configure the jira connection module !url.", array('!url' => l('here', 'admin/config/people/jira_connection'))),
      );
    } else {
      $requirements['jira_connection_config'] = array(
        'value' => t('Configured'),
        'severity' => REQUIREMENT_OK,
      );
    }
    $requirements['jira_connection_config']['title'] = t('Jira connection configuration');
  }

  return $requirements;
}

  /**
   * Implementation of hook_user_update().
   */
function jira_connection_user_insert(&$edit, $account, $category) {
  $jira_url = _jira_connection_create_jira_url();

  $admin_username = variable_get('jira_connection_admin_username', '');
  $admin_password = variable_get('jira_connection_admin_password', '');

  $client = new SoapClient($jira_url);
  try {
    $token = $client->login($admin_username, $admin_password);
  }
  catch (SoapFault $fault) {
    watchdog('jira_connection', 'Error logging in with admin user through SOAP API.', array(), WATCHDOG_ERROR);
    return;
  }

  $jira_user = $client->getUser($token, $account->name);
  if (is_null($jira_user)) {
    $fullname = $account->name;
    if (module_exists('webinos_profile') && property_exists($account, 'profile_main')) {
      $profile_main = $account->profile_main;
      $fullname = $profile_main['field_realname']['und'][0]['value'];
    }
    try {
      $client->createUser($token, strtolower($account->name), user_password(), $fullname, $account->mail);
    }
    catch (SoapFault $falt) {
      watchdog('jira_connection', 'Error creating user !username through SOAP API.', array('!username' => $account->name), WATCHDOG_ERROR);
    }
  }

  try {
    $token = $client->logout($token);
  }
  catch (SoapFault $fault) {
    watchdog('jira_connection', 'Error logging out admin user through SOAP API.', array(), WATCHDOG_ERROR);
  }
}

/**
 * Implementation of hook_user_update().
 */
function jira_connection_user_update(&$edit, $account, $category) {
  $jira_url = _jira_connection_create_jira_url();

  $admin_username = variable_get('jira_connection_admin_username', '');
  $admin_password = variable_get('jira_connection_admin_password', '');

  $group_mapping = array(
    'administrator' => 'jira-administrators',
    'webinos partner' => 'webinos-partners',
    'webinos affiliate' => 'webinos-affiliates',
    'gatekeeper' => 'gatekeepers',
  );

  $client = new SoapClient($jira_url);
  try {
    $token = $client->login($admin_username, $admin_password);
  }
  catch (SoapFault $fault) {
    watchdog('jira_connection', 'Error logging in with admin user through SOAP API.', array(), WATCHDOG_ERROR);
    return;
  }

  $jira_user = $client->getUser($token, $account->name);
  if (is_null($jira_user)) {
    try {
      $jira_user = $client->createUser($token, strtolower($account->name), user_password(), $account->name, $account->mail);
    }
    catch (SoapFault $falt) {
      watchdog('jira_connection', 'Error creating user !username through SOAP API.', array('!username' => $account->name), WATCHDOG_ERROR);
    }
  }

  $new_mail = $edit['mail'];
  $jira_user->email = $new_mail;

  if (module_exists('webinos_profile') && property_exists($account, 'profile_main')) {
    $profile_main = $account->profile_main;
    $fullname = $profile_main['field_realname']['und'][0]['value'];
    if (isset($fullname)) {
      $jira_user->fullname = $fullname;
    }
  }

  try {
    $client->updateUser($token, $jira_user);
    watchdog('jira_connection', 'Updated  user !username through SOAP API.', array('!username' => $account->name), WATCHDOG_DEBUG);
  }
  catch (SoapFault $falt) {
    watchdog('jira_connection', 'Error updating user !username through SOAP API.', array('!username' => $account->name), WATCHDOG_ERROR);
  }

  // Check if user roles were added
  $new_roles = array_diff_key($account->roles, $account->original->roles);
  foreach ($new_roles as $drupal_role_id => $temp_int) {
    $drupal_role = user_role_load($drupal_role_id);
    $drupal_role_name = $drupal_role->name;
    try {
      if (!array_key_exists($drupal_role_name, $group_mapping)) {
        watchdog('jira_connection', 'No drupal group !group_name', array('!group_name' => $drupal_role_name), WATCHDOG_DEBUG);
        continue;
      }
      $jira_group = $client->getGroup($token, $group_mapping[$drupal_role_name]);
      if (!is_null($jira_group) && !empty($jira_group)) {
        try {
          $client->addUserToGroup($token, $jira_group, $jira_user);
          watchdog('jira_connection', 'Added user !username to jira group !group_name through SOAP API.', array('!username' => $account->name,'!group_name' => $group_mapping[$drupal_role_name]), WATCHDOG_DEBUG);
        }
        catch (SoapFault $fault) {
          watchdog('jira_connection', 'Error adding user !username to jira group !group_name through SOAP API.', array('!username' => $account->name,'!group_name' => $group_mapping[$drupal_role_name]), WATCHDOG_ERROR);
        }
      }
    }
    catch (SoapFault $fault) {
      watchdog('jira_connection', 'Error getting group !group_name through SOAP API.', array('!group_name' => $group_mapping[$drupal_role_name]), WATCHDOG_WARNING);
    }
  }

  // Check if user roles were removed
  $removed_roles = array_diff_key($account->original->roles, $account->roles);
  foreach ($removed_roles as $drupal_role_id => $temp_int) {
    $drupal_role = user_role_load($drupal_role_id);
    $drupal_role_name = $drupal_role->name;
    try {
      if (!array_key_exists($drupal_role_name, $group_mapping)) {
        watchdog('jira_connection', 'No drupal group !group_name', array('!group_name' => $drupal_role_name), WATCHDOG_DEBUG);
        continue;
      }
      $jira_group = $client->getGroup($token, $group_mapping[$drupal_role_name]);
      if (!is_null($jira_group) && !empty($jira_group)) {
        try {
          $client->removeUserFromGroup($token, $jira_group, $jira_user);
          watchdog('jira_connection', 'Removed user !username to jira group !group_name through SOAP API.', array('!username' => $account->name,'!group_name' => $group_mapping[$drupal_role_name]), WATCHDOG_DEBUG);
        }
        catch (SoapFault $fault) {
          watchdog('jira_connection', 'Error removing user !username from jira group !group_name through SOAP API.', array('!username' => $account->name,'!group_name' => $group_mapping[$drupal_role_name]), WATCHDOG_ERROR);
        }
      }
    }
    catch (SoapFault $fault) {
      watchdog('jira_connection', 'Error getting group !group_name through SOAP API.', array('!group_name' => $group_mapping[$drupal_role_name]), WATCHDOG_WARNING);
    }
  }

  try {
    $token = $client->logout($token);
  }
  catch (SoapFault $fault) {
    watchdog('jira_connection', 'Error logging out admin user through SOAP API.', array(), WATCHDOG_ERROR);
  }
}

/**
 * Implementation of hook_user_delete().
 */
function jira_connection_user_delete($account) {
  $jira_url = _jira_connection_create_jira_url();

  $admin_username = variable_get('jira_connection_admin_username', '');
  $admin_password = variable_get('jira_connection_admin_password', '');

  $client = new SoapClient($jira_url);
  try {
    $token = $client->login($admin_username, $admin_password);
  }
  catch (SoapFault $fault) {
    watchdog('jira_connection', 'Error logging in with admin user through SOAP API.', array(), WATCHDOG_ERROR);
  }

  $user = $client->getUser($token, $account->name);
  if (!is_null($user)) {
    try {
      $client->deleteUser($token, $account->name);
    }
    catch (SoapFault $fault) {
      watchdog('jira_connection', 'Error deleting user !username through SOAP API.', array('!username' => $account->name), WATCHDOG_ERROR);
    }
  }
}

function _jira_connection_create_jira_url() {
  $jira_url = '';
  if (variable_get('jira_connection_ssl_enabled', FALSE)) {
    $jira_url .= 'https://';
  } else {
    $jira_url .= 'http://';
  }
  $jira_url .= variable_get('jira_connection_server', '');
  $jira_port = variable_get('jira_connection_port', '');
  if (isset($jira_port)) {
    $jira_url .= ':' . variable_get('jira_connection_port', '');
  }
  $jira_url .= variable_get('jira_connection_path', '') . '/rpc/soap/jirasoapservice-v2?wsdl';

  return $jira_url;
}
